import spacy
import pickle 
"""
Code from https://github.com/LARC-CMU-SMU/RecipeGPT-exp/blob/master/utils/spacy_func.py

Credit:
@inproceedings{leeRecipeGPTGenerativePretraining2020,
  title = {{{RecipeGPT}}: {{Generative Pre}}-Training {{Based Cooking Recipe Generation}} and {{Evaluation System}}},
  shorttitle = {{{RecipeGPT}}},
  booktitle = {Companion {{Proceedings}} of the {{Web Conference}} 2020},
  author = {H. Lee and Shu, K. and Achananuparp, R. and Prasetyo, R. Kokoh and Liu, Y. and Lim, E. and Varshney, L.},
  date = {2020-04-20},
  year = 2020,
  pages = {181--184},
  publisher = {{ACM}},
  location = {{Taipei Taiwan}},
  doi = {10.1145/3366424.3383536},
  url = {https://dl.acm.org/doi/10.1145/3366424.3383536},
  urldate = {2020-06-18},
  eventtitle = {{{WWW}} '20: {{The Web Conference}} 2020},
  isbn = {978-1-4503-7024-0},
  langid = {english}
}
"""
class spacy_extension_recipe_gpt:
    def __init__(self):
        self.spacy = spacy.load('en_core_web_lg')
        with open('data/recipeGPT/database.pickle', 'rb') as f:
          self.database = pickle.load(f)

    def ingr(self, lst):
        """ use spacy to process the ingredients

        Args:
          lst: A list of ingredient names

        Returns:
          root_ingr: A list of root nouns in the lemmatized form
          hl: A list that describes whether the input words is a root noun and its highlighting status
          
        """
        hl = [[{'text':x, 'highlight': None} for x in i.split(' ')] for i in lst]
        root_ingr = []
        for i, ingr in enumerate(lst):
            
            # if it is a uni-gram, keep the whole term
            if ' ' not in ingr:
                hl[i][0]['highlight'] = 'wrong'
                doc = self.spacy(ingr)
                if len(doc) > 0:
                  root_ingr.append(doc[0].lemma_)
                
            # if it is 2+ gram, then use spacy
            else:
                phrase = 'Mix the %s and water.'%ingr
                doc = self.spacy(phrase)
                for chunk in doc.noun_chunks:
                    if chunk.text != 'water':
                        for j, word in enumerate(hl[i]):
                            if word['text'] == doc[chunk.end - 1].text:
                                hl[i][j]['highlight'] = 'wrong' 
                                root_ingr.append(doc[chunk.end - 1].lemma_)
        return root_ingr, hl
    
    def instr(self, directions):
        """ use spacy to process the directions
        
        Args:
          lst: A list of ingredient names

        Returns:
          instr: The raw output of spacy
          hl: A list that describes whether the input words is a root noun and its highlighting status
        """
        instr = self.spacy(directions)
        hl = [{'text': token.text, 'highlight': None} for token in instr]
        return instr, hl

    def precision(self,y_true, y_pred):
        if len(y_pred): 
            return len(set(y_true) & set(y_pred))/len(set(y_pred))
        else:
            return 0

    def recall(self,y_true, y_pred):
        if len(y_true):
            return len(set(y_true) & set(y_pred))/len(set(y_true))
        else:
            return 1

    def f1(self,y_true, y_pred):
        precision = self.precision(y_true, y_pred)
        recall = self.recall(y_true, y_pred)
        sum = (precision + recall)
        if sum != 0:
            f1 = 2*precision*recall/sum
        else:
            f1 = 0
        return f1, precision, recall

    def metrics(self, ingredients, instructions):
      if len(instructions) == 0 or len(ingredients) == 0:
        return [0] * 4
      instructions = ' '.join(instructions)
      root_ingr, _ = self.ingr(ingredients)
      instr, _ = self.instr(instructions)
      
      #filter non-ingredients
      root_instr = []
      for chunk in instr.noun_chunks:
          idx_rootnoun = chunk.end - 1
          str_rootnoun = instr[idx_rootnoun].lemma_
          if str_rootnoun in root_ingr:
            root_instr.append(str_rootnoun)
          elif str_rootnoun in self.database:
            root_instr.append(str_rootnoun)

      root_ingr, root_instr = set(root_ingr), set(root_instr)
      intersection = root_ingr.intersection(root_instr)
      union = root_ingr.union(root_instr)
      if len(union) != 0:
        jacc = len(intersection)/len(union)
      else:
        jacc = 0
      f1, precision, recall = self.f1(root_ingr, root_instr)
      return jacc, f1, precision, recall

if __name__ == "__main__":
  from recipes_loader import RecipesLoader
  sp = spacy_extension_recipe_gpt()

  loader = RecipesLoader(partition="test", folder_path="/mnt/data/study/Diplomarbeit/code/data/recipe1M/cleaned_ingredients/ingredient_occurence_score/pmi/instruction_encoding_length/sentence_switched/new_pmi/ny_ingredients/")
  for i, r in enumerate(loader.yield_recipes()):
    print("Jacc", sp.calc_jaccard(r))
    if i == 100:
      break