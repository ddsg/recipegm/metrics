import re

def clean_instructions(instructions):
    return clean(" ".join([sentence.replace("!", ".").replace("?", ".") for sentence in instructions]))

def clean(text):
    return re.sub(r"[@~\+#{}\[\]\^&:/;\d\"\.,'\(\)\*]", "", text).lower().replace("--", "").split()

if __name__ == "__main__":
    from recipes_loader import RecipesLoader
    import argparse
    import os
    from tqdm import tqdm
    from collections import Counter
    import pandas as pd

    parser = argparse.ArgumentParser()
    parser.add_argument('--path', type=str, required=True)
    parser.add_argument('--partitions', type=str, required=True)
    args = parser.parse_args()
    assert os.path.exists(args.path) and os.path.isdir(args.path), f"Path does not exist! '{args.path}'"

    unigrams = []
    for p in args.partitions.split():
        loader = RecipesLoader(partition=p, folder_path=args.path)
        for i, r in enumerate(tqdm(loader.yield_recipes())):
            unigrams.extend(clean_instructions([i["text"] for i in r["instructions"]]))

    df = pd.DataFrame.from_dict(Counter(unigrams), orient='index').reset_index()
    df = df.rename(columns={"index": "unigram", 0: "count"})
    df["probability"] = df["count"] / len(unigrams)
    df = df.sort_values(by=['count']).set_index("unigram")
    print(df.head())
    df.to_csv("data/unigram_probability.csv")


            