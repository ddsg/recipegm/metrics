import json
from pprint import pprint
import spacy
from flashtext import KeywordProcessor
import os

class IngredientExtractor:
    def __init__(self, keywords=None, ingredients_path="data/ingredients/"):
        self.spacy = None
        self.keyword_processor = None

        print("Loadig ingredient extractor...")
        if keywords is None:
            path = os.path.join(ingredients_path, "det_ingrs_valid_ingredients_list.json")
            with open(path) as f:
                valid_det_ingrs = json.load(f)

            path = os.path.join(ingredients_path, "ingredients_from_foodKG.txt")
            with open(path) as f:
                foodKG_ingredients = [line.rstrip('\n') for line in f.readlines()]

            path = os.path.join(ingredients_path, "more_words.txt")
            with open(path) as f:
                more_ingredients = [line.rstrip('\n') for line in f.readlines()]

            ingredients = list(map(lambda x: x.lower(),set(foodKG_ingredients + valid_det_ingrs + more_ingredients)))
        else:
            ingredients = list(map(lambda x: x.lower(),set(keywords)))
        self.spacy = spacy.load("en_core_web_lg")
        docs = list(self.spacy.pipe(ingredients))
        ingredients_tokenized = [self.tokenize(doc, isDoc=True) for doc in docs]
        ingredient_token_phrases = [ " ".join(tokens) for tokens in ingredients_tokenized]
        self.keyword_processor = KeywordProcessor()
        self.keyword_processor.add_keywords_from_list(ingredient_token_phrases)
        print(f"Extractor loaded {len(self.keyword_processor.get_all_keywords())} tokenized ingredient phrases.")

    def tokenize(self, text, isDoc=False, return_lemma=False):
        doc = text if isDoc else self.spacy(text)
        return [token.lemma_ if return_lemma else token.text for token in doc if not (token.is_stop or token.is_punct or token.like_num or token.is_bracket)]

    def extract(self, text, return_lemma=False):
        tokens = self.tokenize(text, return_lemma)
        tokens = " ".join(tokens)
        match = self.keyword_processor.extract_keywords(tokens)
        return match 
