import contextlib
import os
import json
from enum import Enum
from os.path import isfile

class RecipesLoader:
    def __init__(self, partition, folder_path):
        self.partition = partition
        self.base_path = folder_path
        assert os.path.exists(self.base_path), f"Given path to directory does not exists! '{self.base_path}'"
        self.recipes = None
        self.filename = None
        self._find_file()

    def _find_file(self):
        recipes = []
        files = [os.path.join(self.base_path, f) for f in os.listdir(self.base_path) if os.path.isfile(os.path.join(self.base_path, f))]
        for file in files:
            if self.partition in file.split("/")[-1] and file.endswith(".json"):
                self.filename = os.path.basename(file)
                self.abs_filename = file
        return self.filename, self.partition, self.abs_filename

    def load_recipes(self):
        print(f"Loading recipes ({self.partition}) ...")
        with open(self.abs_filename, "r") as f:
            self.recipes = json.load(f)
            print(f"{len(self.recipes)} recipes loaded.")
        assert self.recipes is not None, f"Found no JSON files of partition {self.partition}"

    def yield_recipes(self):
        """Automatically uses generator to yield recipes if they were dumped each in a seperate line. See 'dict_per_line' in dump_recipes method"""
        print("", flush=True)
        print(f"Yielding recipes ({self.partition}) ...")

        with open(self.abs_filename, "r") as f:
            for line in f:
                line = line.rstrip()
                if line[0] == '[' and line[-1] == ']':
                    recipes = json.loads(line)
                    for r in recipes:
                        yield r
                else:
                    if line[0] == '[':
                        line = line[1:]
                    if line[-1] == ',' or line[-1] == ']':
                        line = line[:-1]
                    yield json.loads(line)

    def dump_recipes(self, recipes, path, filename, dict_per_line=True):
        """Dumps recipes line-wise as JSON array if dict_per_line is true, else dumps all recipes in first line"""
        dump_path = os.path.join(path, filename)
        self.dump_file = dump_path
        with open(dump_path, "w") as f:
            print(f"Dumping '{dump_path}'")
            if dict_per_line == True:
                f.write('[' + ',\n'.join(json.dumps(r) for r in recipes) + ']\n')
            else:
                json.dump(recipes, f)

    def get_filename(self):
        return self.filename

    def get_recipes(self):
        if self.recipes is None:
            self.load_recipes()
        return self.recipes

    @contextlib.contextmanager
    def dumper(self, path, filename):
        """Dump a recipe to a file in an iterative manner. If the file is empty or contains a valid array of json objects it is overridden (new file). Otherwise the recipe is appended. If the recipe is none the JSON array gets closed
                Example usage:
                    with loader.dumper(".", "file.json") as dumper:
                        for r in yield_recipes():
                            dumper.dump_recipe(r)
        """
        class Dumper():
            class FileMode(Enum):
                append_file = 2,
                new_file = 3
                none = 4

            def __init__(self, dump_file_path):
                self.file_mode = self.FileMode.none
                self.append_fp = None
                self.dump_file = dump_file_path

            def _determine_file_mode(self):
                if not os.path.exists(self.dump_file):
                    self.file_mode = self.FileMode.new_file
                    return
                assert os.path.isfile(self.dump_file), "Given path must point to a file!"
                with open(self.dump_file, "r") as f:
                    gen = (line for i, line in enumerate(f))
                    line = None
                    try:
                        while True:
                            line = next(gen)
                    except StopIteration:
                        pass
                    finally:
                        del gen
                    if line is not None:
                        line = line.rstrip()
                        if line[-1] == ']':
                            self.file_mode = self.FileMode.new_file
                        elif line[-1] == ',':
                            self.file_mode = self.FileMode.append_file
                            self.append_fp = open(self.dump_file, "a")
                    else:
                        self.file_mode = self.FileMode.new_file

            def dump_recipe(self, recipe):
                assert recipe is not None, "Not None recipe must be given!"
                if self.file_mode is not self.FileMode.append_file:
                    self._determine_file_mode()

                if self.file_mode == self.FileMode.new_file:
                    with open(self.dump_file, "w") as f:
                        f.write('[' + json.dumps(recipe) + ",\n")
                elif self.file_mode == self.FileMode.append_file:
                    self.append_fp.write(json.dumps(recipe) + ",\n")
                
            def _close_dump(self):
                if self.append_fp is None:
                    return
                self.append_fp.close()
                # delete last ',\n'
                with open(self.dump_file, "ab") as f:
                    f.seek(0, 2)
                    f.seek(-2, 2)
                    f.truncate()
                # close dump
                with open(self.dump_file, "a") as f:
                    f.write(']\n')

        dumper = Dumper(os.path.join(path, filename))
        yield dumper
        dumper._close_dump()

if __name__ == "__main__":
    import argparse
    from pprint import pprint

    parser = argparse.ArgumentParser()
    parser.add_argument("--path", default="data/for_dev/", type=str, help="Path to files of recipes")
    parser.add_argument("--partition", default="test", type=str, help="Partition test/train/val of recipe file (must be in filename)")
    args = parser.parse_args()

    loader = RecipesLoader(args.partition, args.path)
    for _ in range(3):
        pprint(next(loader.yield_recipes()))