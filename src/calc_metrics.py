import argparse
import os
from tqdm import tqdm
import pandas as pd
from Metrics import Metrics

parser = argparse.ArgumentParser()
parser.add_argument('--path', type=str, required=True)
parser.add_argument('--out', type=str, required=True)
parser.add_argument('--n', type=int, default=10)
args = parser.parse_args()

assert os.path.exists(args.out) and os.path.isdir(args.out), f"Path does not exist! '{args.out}'"
assert os.path.exists(args.path) and os.path.isdir(args.path), f"Path does not exist or is not a directory! '{args.path}'"
results = []
metrics = Metrics()
files = sorted([os.path.join(args.path, f) for f in os.listdir(args.path) if os.path.isfile(os.path.join(args.path, f))])
old_id = None
pbar = tqdm(files)
for file in pbar:
    _, id, _, k = splitter = os.path.basename(file)[:-4].split("_")
    pbar.set_description(f"Recipe id {id}")
    k = int(k)
    with open(file, "r") as f:
        lines = f.readlines()
    
    for i, line in enumerate(lines):
        parts = line.split(";", 1)
        # ingredients are the same for all recipes of the same id
        if id != old_id:
            if len(parts[0]) > 0:
                ingredients = parts[0].split("$")
            else:
                ingredients = []
        
        if len(parts[1]) > 0:
            instructions = parts[1].strip().split(".")
        else:
            instructions = []
        mean_log_unigram_probability = metrics.mean_log_unigram_probability(instructions)
        n_gram_repetition = metrics.n_gram_repetition(instructions)
        jacc, f1, precision, recall = metrics.ingredient_metrics(ingredients, instructions)
        result = (id,k, mean_log_unigram_probability, n_gram_repetition, jacc, f1, recall, precision)
        
        if k == 1:
            # with k == 1 the instructions don't change
            results.extend([result] * args.n)
            break
        else:
            results.append(result)
        old_id = id
        
df = pd.DataFrame.from_records(results, columns="recipe1M_id top_k mean_log_unigram_probability n_gram_repetition jacc f1 recall precision".split())
print(df.head())

basename = os.path.basename(os.path.normpath(args.path)).replace(" ", "_")
df.to_csv(os.path.join(args.out, basename + "_metrics_results.csv"), index=False)