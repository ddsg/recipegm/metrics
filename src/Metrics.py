
from spacy_func import spacy_extension_recipe_gpt
from add_n_gram_repetition import n_gram_repeptition
import pandas as pd
from count_unigrams import clean_instructions
import numpy as np


class Metrics():

    def __init__(self):
        self.unigram_probabilities = pd.read_csv("data/unigram_probability.csv", index_col=0)
        self.sp = spacy_extension_recipe_gpt()        

    def mean_log_unigram_probability(self, instructions):
        unigrams = clean_instructions(instructions)
        unigrams = list(filter(lambda x: x in self.unigram_probabilities.index, unigrams))
        if len(unigrams) == 0:
            return None
        probabilities = self.unigram_probabilities.loc[unigrams, "probability"]
        return np.log10(probabilities).mean()

    def n_gram_repetition(self, instructions):
        return n_gram_repeptition(instructions)

    def ingredient_metrics(self, ingredients, instructions):
        jacc, f1, precision, recall = self.sp.metrics(ingredients, instructions)
        return jacc, f1, precision, recall

    def all_metrics(self, ingredients, instructions):
        mean_log_unigram_probability = self.mean_log_unigram_probability(instructions)
        n_gram_repetition = self.n_gram_repetition(instructions)
        jacc, f1, precision, recall = self.ingredient_metrics(ingredients, instructions)
        return (mean_log_unigram_probability, n_gram_repetition, jacc, f1, recall, precision)

if __name__ == "__main__":        
    def tests():
        metrics = Metrics()
        from recipes_loader import RecipesLoader
        
        #edge cases
        print(metrics.mean_log_unigram_probability([]))
        print(metrics.n_gram_repetition([]))
        print(metrics.ingredient_co_occurence_pmi([]))
        print(metrics.ingredient_co_occurence_pmi(["pizza sauce", "pizza sauce", "pepperoni", "mozzarella cheese"]))

        # normal usage
        print(metrics.mean_log_unigram_probability(["mix salt and oil."]))
        print(metrics.n_gram_repetition(["mix salt and oil"]))
        print(metrics.n_gram_repetition(["mix salt and oil.", "mix salt and oil."]))
        print(metrics.n_gram_repetition(["mix salt and oil.", "mix oil and oil and oil."]))        
        r = next(RecipesLoader("val", "data/recipe1M/").yield_recipes())
        ingredients =  r["cleaned ingredients"]
        instructions = [i["text"] for i in r["instructions"]]
        print(metrics.ingredient_metrics(ingredients, instructions))

        # edge cases recipe1M
        print(metrics.ingredient_metrics(ingredients, []))
        print(metrics.ingredient_metrics([], instructions))
        print(metrics.ingredient_metrics([], []))
        jacc, f1, _, _ = metrics.ingredient_metrics(["salt", "oil", "vinegar"], ["salt"])
        print("Jacc.: harder punishment for bad recipes:", "jacc", jacc, "f1", f1)
        print("true", ["salt", "oil", "vinegar"], "predicted", ["salt"])

    tests()
