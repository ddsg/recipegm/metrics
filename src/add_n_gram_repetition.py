from nltk.util import ngrams 

def n_gram_repeptition(instructions):
    scores = []
    instructions = " ".join([sentence.replace("!", ".").replace("?", ".") for sentence in instructions])
    sentences = [x.strip() for x in instructions.split(".") if len(x.strip()) != 0]
    for sentence in sentences:
        words = sentence.split()
        n_grams = []
        for n in range(1,5):
            n_grams.extend(list(ngrams(words, n)))
        if len(n_grams) > 0:
            score = len(set(n_grams)) / len(n_grams)
            scores.append(score)
    return sum(scores)/len(scores) if len(scores) > 0 else None

if __name__ == "__main__":

    from recipes_loader import RecipesLoader
    import argparse
    import os
    from tqdm import tqdm
    import spacy

    parser = argparse.ArgumentParser()
    parser.add_argument('--path', type=str, required=True)
    parser.add_argument('--partitions', type=str, required=True)
    parser.add_argument('--out', type=str, required=True)
    args = parser.parse_args()
    assert args.out != args.path, "In and out paths must not be equal, due to interativley dumping the recipes!"
    assert os.path.exists(args.out) and os.path.isdir(args.out), f"Path does not exist! '{args.out}'"
    assert os.path.exists(args.path) and os.path.isdir(args.path), f"Path does not exist! '{args.path}'"

    sp = spacy.load("en_core_web_lg")
    for p in args.partitions.split():
        loader = RecipesLoader(partition=p, folder_path=args.path)
        with loader.dumper(args.out, loader.filename) as dumper:
            for i, r in enumerate(tqdm(loader.yield_recipes())):
                r["n_gram_repetition"] = n_gram_repeptition([i["text"] for i in r["instructions"]])
                dumper.dump_recipe(r)