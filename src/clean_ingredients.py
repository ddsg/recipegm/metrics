import argparse
from json import dump
import os
from ingredient_extractor import IngredientExtractor
from recipes_loader import RecipesLoader
from tqdm import tqdm
from utils import * 

def clean_frequent_ingredients(ingredients):
    new_ingrs = []
    for ingr in ingredients:
        if len(ingr) == 0:
            continue
        
        if ingr[0] == "fresh" or ingr[0] == "small" or ingr[0] == "kosher":
            ingr = ingr[1:]
        if isinstance(ingr, list) and not isinstance(ingr, str):
            ingr = " ".join(ingr)
        if "salt" in ingr and "pepper" in ingr:
            new_ingrs.append("salt")
            new_ingrs.append("pepper")
        elif ingr == "ground pepper" or ingr == "ground black pepper":
            new_ingrs.append("pepper")
        elif "sea salt" in ingr:
            new_ingrs.append("salt")
        elif "garlic" in ingr and "clove" in ingr:
            new_ingrs.append("garlic")
        elif "olive oil" in ingr:
            new_ingrs.append("olive oil")
        elif ingr == "purpose flour" or ingr == "wheat flour":
            new_ingrs.append("flour")
        elif ingr == "":
            continue
        elif ingr == "granulated sugar" or "white sugar" == ingr:
            new_ingrs.append("sugar")
        elif "butter" in ingr and "magarine" in ingr:
            new_ingrs.append("butter")
        elif ingr =="ground nutmeg":
            new_ingrs.append("nutmeg")
        elif ingr == "ground cinnamon":
            new_ingrs.append("cinnamon")
        elif "cheddar cheese" in ingr:
            new_ingrs.append("cheddar cheese")
        else:
            new_ingrs.append(ingr)
    return unique(new_ingrs)

def clean_ingredients(extractor, ingredients, do_extract=True, return_lemma=False):
    if do_extract:
        ingredients = [extractor.extract(ingr, return_lemma) for ingr in ingredients]
    ingredients = clean_frequent_ingredients(ingredients)
    return unique(ingredients)

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--out', type=str, required=True)
    parser.add_argument('--path', type=str, required=True)
    parser.add_argument('--partitions', type=str, required=True)
    args = parser.parse_args()
    assert os.path.exists(args.out) and os.path.isdir(args.out), f"Path does not exist! '{args.out}'"
    assert os.path.exists(args.path) and os.path.isdir(args.path), f"Path does not exist! '{args.path}'"

    extractor = IngredientExtractor()
    for p in args.partitions.split():
        loader = RecipesLoader(partition=p, folder_path=args.path)
        recipes = loader.get_recipes()
        ratings = []
        scores = []
        for i, r in enumerate(tqdm(recipes)):
            ingredients = [ingr["text"] for ingr in r["ingredients"]]
            r["cleaned_ingredients"] = clean_ingredients(extractor, ingredients)
            if i % 5000 == 0 and i != 0:
                dump(recipes, args.out, loader.filename)
        dump(recipes, args.out, loader.filename)