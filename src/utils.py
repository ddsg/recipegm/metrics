import json
import os
import itertools

def dump(recipes, path, filename):
    dump_path = os.path.join(path, filename)
    with open(dump_path, "w") as f:
        print(f"Dumping '{dump_path}'")
        json.dump(recipes, f)

def unique(sequence):
    seen = set()
    return [x for x in sequence if not (x in seen or seen.add(x))]

def flatten(array_of_arrays):
    return list(itertools.chain.from_iterable(array_of_arrays))

def grouper(iterable, n, fillvalue=None):
    "Collect data into fixed-length chunks or blocks"
    # grouper('ABCDEFG', 3, 'x') --> ABC DEF Gxx"
    args = [iter(iterable)] * n
    return itertools.zip_longest(*args, fillvalue=fillvalue)