# Metrics of RecipeGM
## Usage
1. In a new virtual environment install the python modules with `pip install -r requirements.txt`
2. Start the example ```python src/calc_metrics.py --path generated_recipes/ --out .``` This will read in the generated recipe files in "generated_recipes/" and calculate for each line (a recipe) within each file all metrics (mean_log_unigram_probability, n_gram_repetition, jacc, f1, recall, precision). After processing, the results are recorded within a csv file (recipe1M_id, top-k, \[metrics]). The result can be viewed in "metrics/generated_recipes_metrics_results.csv".
3. The calculated scores are to average for each *k* to obtain results as presented in the paper

## Expected File Format
 The expected file format can be viewed in "metrics/generated_recipes/".
 Filename: recipe\_[x]\_k\_[k].txt wit *x* as the recipe1M id and *k* as the top-k sampling parameter during generation.
 Within each file may be *n* generated recipes with each recipe on a seperate line. However, if there are more or less a warning will appear, but all of them will be calculated.
 For files with *k*=1 the script calculates only the metrics for the first line and duplicates the results *n* times for performance reasons. *n* can be set by the flag "--n" (Can be omitted, default is "--n 10")
 Each line / the recipe is formatted as follows: *ingredients*,*instructions*, whereas *ingredientes* is a '$'-seperated (see RecipeGPT) string of ingredient phrases (each word of a phrase is seperated by the space character).

